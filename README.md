# Spring ORM
Premier projet Spring boot utilisant JPA/Spring Data pour la partie entity et repository

[Version du code sans pagination sur ce commit](https://gitlab.com/simplonlyon/promo18/spring-orm/-/tree/no-pagination)

## Configuration d'un projet JPA
On crée les entités avec les annotations spring nécessaires 
```java

@Entity
public class Example {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    
    private String prop;

    
    //getter/setter et constructeurs
```

on indique dans le application.properties la bdd et le comportement de jpa pour la génération de la bdd
```properties
#le createDatabaseIfNotExist permet de créer la base de données automatiquement
spring.datasource.url=jdbc:mysql://simplon:1234@localhost:3306/db_example?createDatabaseIfNotExist=true

# Dit à JPA de mettre à jour les tables en se basant sur les entités
spring.jpa.hibernate.ddl-auto=update
# Affiche les requêtes SQL générée par JPA dans la console, utile pour le débug
spring.jpa.show-sql=true
```

On crée le repository
```java
@Repository
public interface ExampleRepository extends JpaRepository<Example, Integer> {
    //On peut rajouter des méthodes spécifiques si besoin, mais par défaut, on aura un crud complet
}

```


## Exercices
### Première entité et repo
1. Créer un projet spring boot qu'on va appeler springorm en lui mettant comme dépendances : spring devtools, mysql driver, spring data jpa et spring web
2. Dans ce projet, créer une entité Dog avec un id, un name, un breed et un birthdate
3. Créer une interface DogRepository et lui faire extends de JpaRepository<Dog,Integer> pour créer un repo complet avec tout le crud
4. Créer un contrôleur DogController et dedans faire un Autowired du DogRepository qu'on vient de "créer"
5. Dans ce contrôleur, utiliser les méthodes du DogRepository pour faire les différentes méthodes de l'API rest (donc un getAll sur /api/dog, un getById sur /api/dog/id, etc.)

### Rajouter des dresseur·euses et des concours
1. Créer un entité Trainer qui aura un name, un id, une city
2. Créer une entité Contest qui aura un id, une date, un type (en string)
3. Dans le Trainer, rajouter une liste de Dog et dans le Dog rajouter un Trainer, puis chercher sur internet comment on fait un OneToMany avec Jpa (sachant que là ça sera OneToMany côté Trainer et ManyToOne côté Dog)
4. Dans le Dog, rajouter une List de Contest et dans le Contest une List de Dog puis refaire la même chose qu'avant mais cette fois ci pour un ManyToMany (des deux côtés)

### Contest Contrôleur
1. Créer un ContestRepository et lui faire hériter de JpaRepository comme on a fait pour le DogRepository
2. Créer le ContestController dans lequel vous faites un autowired du repo
3. Créer une route GET /api/contest qui va renvoyer tous les contests et leurs chiens (il va falloir rajouter un JsonIgnore sur un des deux côtés de la relation manytomany)
4. Rajouter une route POST /api/contest qui va attendre un contest en requestbody et le faire persister avec le save
5. Côté DogController, rajouter une méthode PATCH qui va ressembler à /api/dog/{idDog}/contest/{idContest} et qui va donc avoir 2 pathvariable, se servir de ces deux id pour aller chercher le chien ainsi que le contest, puis ajouter le contest dans la la liste des contests du chiens et faire un save de ce dernier

### Afficher les chiens par race et recherche
1. Dans le DogRepository, en vous basant sur la documentation de spring data (https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#repositories.query-methods.query-creation), créer une méthode permettant de récupérer une liste de chien par leur breed
2. Dans le DogController, dans la méthode getAll, rajouter un RequestParam optionnel pour la breed, et s'il est présent, déclencher la méthode que vous venez de faire, sinon, déclencher le findAll classique
3. Rajouter une autre méthode dans le repository qui cette fois ci renverra une liste de chien en se basant sur la breed ou le name en utilisant un like, puis modifier la méthode getAll pour faire que si le RequestParam optionnel "search" est présent, alors on déclenche cette méthode du repo

### Pagination des chiens
1. Chercher sur internet pagination jparepository et vous devriez trouver un exemple qui consiste à donner un argument au findAll dans lequel on lui indique le nombre d'élément par page et la page actuelle. Appliquer ça sur le findAll dans le contrôleur et regarder si ça marche
2. Rajouter des RequestParam optionnels page et pageSize en leur mettant comme valeur par défaut respectivement 0 et 10 (première page, et 10 élément par page)
3. Utilisez ces paramètre dans le findAll, et vous devriez maintenant pouvoir afficher les pages de chiens en allant sur http://localhost:8080/api/dog?page=2 par exemple, et changer le nombre de chien à afficher en rajouter &pageSize=20