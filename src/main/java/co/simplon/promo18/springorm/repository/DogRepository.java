package co.simplon.promo18.springorm.repository;

import java.util.List;

import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import co.simplon.promo18.springorm.entity.Dog;

@Repository
public interface DogRepository extends JpaRepository<Dog, Integer> {
    /**
     * Méthode autogénérée par spring data qui va récupérer la liste des chiens en
     * se basant sur leur race
     * @param breed la race de chien dont on veut récupérer les membres
     * @return une liste de chiens correspondant à la race donnée
     */
    List<Dog> findByBreed(String breed);

    List<Dog> findByNameContainingOrBreedContaining(String name, String breed);

    @Query("SELECT d FROM Dog d WHERE d.breed LIKE %:searchTerm% OR d.name LIKE %:searchTerm%")
    List<Dog> search(String searchTerm);
}
