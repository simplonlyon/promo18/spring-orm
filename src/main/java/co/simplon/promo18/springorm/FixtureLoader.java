package co.simplon.promo18.springorm;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import com.github.javafaker.Faker;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import co.simplon.promo18.springorm.entity.Contest;
import co.simplon.promo18.springorm.entity.Dog;
import co.simplon.promo18.springorm.entity.Trainer;

/**
 * Une fixture est un fichier qui va servir à peupler une base de données,
 * elle peut soit être lancée au lancement de l'appli comme c'est le cas ici, ou
 * bien à la demande en le mettant dans un CommandLineRunner
 * C'est un composant optionnel, qui permet à tout le monde d'avoir une base de données
 * à peu près similaire, on peut lui substituer un fichier sql qui fait des inserts
 */
@Component
public class FixtureLoader {
    @PersistenceContext
    private EntityManager em;

    @EventListener(ApplicationReadyEvent.class)
    @Transactional
    public void load() {
        Faker faker = new Faker();
        
        List<Contest> contestList = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            Contest contest = new Contest(
                faker.date().future(10, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDate(),
                faker.app().name()
            );
            em.persist(contest);
            contestList.add(contest);
        }

        for (int x = 1; x <= 5; x++) {
            Trainer trainer = new Trainer(faker.name().fullName(), faker.address().city());
            em.persist(trainer);
            for (int i = 1; i <= 10; i++) {
                Dog dog = new Dog(faker.dog().name(), faker.dog().breed(), LocalDate.of(2022, 5, i));
                dog.setTrainer(trainer);
                contestList.get(new Random().nextInt(10)).getDogs().add(dog);
                em.persist(dog);
            }
        }
    }
}
