package co.simplon.promo18.springorm.entity;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
// @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id") // Une autre manière d'éviter les boucles infinies, cette méthode fera en sorte d'afficher uniquement l'id de la classe 
public class Contest {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @NotNull
    private LocalDate date;
    @NotBlank
    private String type;

    @ManyToMany
    @JsonIgnore
    private Set<Dog> dogs = new HashSet<>();
    
    public Contest(LocalDate date, String type) {
        this.date = date;
        this.type = type;
    }
    public Contest(Integer id, LocalDate date, String type) {
        this.id = id;
        this.date = date;
        this.type = type;
    }
    public Contest() {
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public LocalDate getDate() {
        return date;
    }
    public void setDate(LocalDate date) {
        this.date = date;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public Set<Dog> getDogs() {
        return dogs;
    }
    public void setDogs(Set<Dog> dogs) {
        this.dogs = dogs;
    }
}
