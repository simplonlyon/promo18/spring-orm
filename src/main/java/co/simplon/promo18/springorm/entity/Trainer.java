package co.simplon.promo18.springorm.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
public class Trainer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String city;

    // @JsonIgnoreProperties("trainer") //Si on veut quand même aller chercher les chiens mais pas aller chercher le trainer des chiens qu'on va chercher
    @JsonIgnore
    @OneToMany(mappedBy = "trainer") //En général, on mettra toujours le mappedBy sur un OneToMany pour dire à quel propriété de l'autre table est liée cette relation
    private List<Dog> dogs = new ArrayList<>();
    
    public Trainer() {
    }
    public List<Dog> getDogs() {
        return dogs;
    }
    public void setDogs(List<Dog> dogs) {
        this.dogs = dogs;
    }
    public Trainer(String name, String city) {
        this.name = name;
        this.city = city;
    }
    public Trainer(Integer id, String name, String city) {
        this.id = id;
        this.name = name;
        this.city = city;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }
}
