package co.simplon.promo18.springorm.entity;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PastOrPresent;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
public class Dog {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    
    @NotBlank
    private String name;
    private String breed;
    @PastOrPresent
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthdate;

    @ManyToOne
    @JsonIgnoreProperties("dogs") //Dit au convertisseur json d'ignorer la propriété "dogs" du trainer du chien
    private Trainer trainer;

    @ManyToMany(mappedBy = "dogs")
    @JsonIgnore //Permet d'ignorer la liste de contest au moment de convertir l'objet en json, ça permet d'éviter les requêtes inutiles, voir les boucles infinies
    private Set<Contest> contests = new HashSet<>(); //Ici on fait des Set plutôt que des List afin qu'il ne puisse pas y avoir plusieurs fois le même contest dans un même chien
    
    public Dog(String name, String breed, LocalDate birthdate) {
        this.name = name;
        this.breed = breed;
        this.birthdate = birthdate;
    }
    public Set<Contest> getContests() {
        return contests;
    }
    public void setContests(Set<Contest> contests) {
        this.contests = contests;
    }
    public Trainer getTrainer() {
        return trainer;
    }
    public void setTrainer(Trainer trainer) {
        this.trainer = trainer;
    }
    public Dog(Integer id, String name, String breed, LocalDate birthdate) {
        this.id = id;
        this.name = name;
        this.breed = breed;
        this.birthdate = birthdate;
    }
    public Dog() {
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getBreed() {
        return breed;
    }
    public void setBreed(String breed) {
        this.breed = breed;
    }
    public LocalDate getBirthdate() {
        return birthdate;
    }
    public void setBirthdate(LocalDate birthdate) {
        this.birthdate = birthdate;
    }
}
