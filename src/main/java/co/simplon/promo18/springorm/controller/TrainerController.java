package co.simplon.promo18.springorm.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.simplon.promo18.springorm.entity.Trainer;
import co.simplon.promo18.springorm.repository.TrainerRepository;

@RestController
@RequestMapping("/api/trainer")
public class TrainerController {
    
    @Autowired
    private TrainerRepository repo;


    @GetMapping
    public List<Trainer> getAll() {
        return repo.findAll();
    }
}
