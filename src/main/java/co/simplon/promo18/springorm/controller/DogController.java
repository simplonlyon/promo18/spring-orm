package co.simplon.promo18.springorm.controller;

import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.promo18.springorm.entity.Contest;
import co.simplon.promo18.springorm.entity.Dog;
import co.simplon.promo18.springorm.repository.ContestRepository;
import co.simplon.promo18.springorm.repository.DogRepository;

@RestController
@RequestMapping("/api/dog")
public class DogController {

    @Autowired
    private DogRepository repo;
    @Autowired
    private ContestRepository repoContest;

    /**
     * Recherche non paginée qui renvoie une list de chien en se basant sur si leur
     * name ou leur breed contient le term recherché
     * @param term Le terme recherché
     * @return une liste de chien non paginée
     */
    @GetMapping("/search/{term}")
    public List<Dog> search(@PathVariable String term) {
        return repo.search(term);
    }

    /**
     * Affiche les chiens d'une race donnée
     * @param breed La race dont on veut afficher les chiens
     * @return La liste des chiens non paginée
     */
    @GetMapping("/breed/{breed}")
    public List<Dog> getByBreed(@PathVariable String breed) {
        return repo.findByBreed(breed);
    }

    /**
     * Méthode qui permet de récupérer une liste de chien paginée à laquelle on donne
     * la page à afficher et le nombre d'élément par page
     * Accessible via http://localhost:8080/api/dog?page=2&pageSize=15 par exemple (mais la page et pageSize sont optionnels)
     * @param page Page à afficher, 0 par défaut
     * @param pageSize Nombre d'élément par page, 10 par défaut
     * @return Une Page de chien contenant non seulement les chiens, mais aussi toutes les informations de pagination (total de page, nombre d'élément total etc.)
     */
    @GetMapping
    public Page<Dog> getAll(
    @RequestParam(required = false, defaultValue = "0") int page,
    @RequestParam(required = false, defaultValue = "10") int pageSize) {
        return repo.findAll(PageRequest.of(page, pageSize));
    }

    @GetMapping("/{id}")
    public Dog getOne(@PathVariable int id) {
        // Optional<Dog> dog = repo.findById(id);
        // if(dog.isEmpty()) {
        //     throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        // }
        
        // return dog.get();

        Dog dog = repo.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        return dog;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Dog add(@Valid @RequestBody Dog dog) {
        dog.setId(null);
        return repo.save(dog);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id) {
        Dog toDelete = getOne(id); //On récupère le chien avec la méthode getOne, comme ça, l'algo de renvoyer un 404 est déjà faite
        repo.delete(toDelete);
    }

    @PutMapping("/{id}")
    public Dog update(@PathVariable int id, @Valid @RequestBody Dog dog) {
        Dog toUpdate = getOne(id);
        toUpdate.setName(dog.getName());
        toUpdate.setBreed(dog.getBreed());
        toUpdate.setBirthdate(dog.getBirthdate());
        return repo.save(toUpdate);
    }

    @PatchMapping("/{idDog}/contest/{idContest}")
    public Dog registerDogToContest(@PathVariable int idDog, @PathVariable int idContest) {
        Dog toUpdate = getOne(idDog);
        Contest contest = repoContest.findById(idContest)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

        contest.getDogs().add(toUpdate);
        repoContest.save(contest);

        return toUpdate;
    }

    @GetMapping("/{id}/contest")
    public Set<Contest> getContests(@PathVariable int id) {
        Dog dog = getOne(id);
        return dog.getContests();
    }
}
